'use strict'

/*
Теорія

1. Які існують типи даних у Javascript?

String, Number, BitInt, Boolean, Symbol, Object, Null, Undefined

2. У чому різниця між == і ===?

== - порівняння без врахування типу ( 1 == "1" // true);
=== - порівняння з урахуванням типу ( 1 === "1" // false);

3. Що таке оператор?

Оператор - це команда у вигляді символу (або групи символів), що ініціює операцію з даними
(математичні дії, порівняння тощо).

*/

console.log('Task 1')

let userName = prompt('What is your name?');
let userAge = prompt('How old are you?');

if (!userName || (isNaN(+userAge) || +userAge < 0)) {
    while (!userName || (isNaN(+userAge) || +userAge < 0)) {
        userName = prompt('What is your name?', userName);
        userAge = prompt('How old are you?', userAge);
    }
}

if (userAge < 18) {
    alert('You are not allowed to visit this website!');
} else if (userAge < 23) {
    let approve = confirm('Are you sure you want to continue?');
    !!approve ? alert(`Welcome, ${userName}`) : alert('You are not allowed to visit this website!');
} else {
    alert(`Welcome, ${userName}`);
}
